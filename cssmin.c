/*
  cssmin: simple CSS minifier
  Copyright (c) 2017 Aaron Trickey <aaron@casualprogrammer.com>

  Usage:
    cssmin < input.css > output.css

  Refer to README.md in this directory for details.
 */

#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#define MAXLINE 1024

enum state {
  state_normal,
  state_hws,
  state_dquote,
  state_squote,
  state_bquote_in_dquote,
  state_bquote_in_squote,
  state_startcomment,
  state_comment,
  state_endcomment,
};

// horizontal whitespace, according to CSS, is limited to just these characters
static bool is_hws(int ch)
{
  return ch == ' ' || ch == '\t';
}

int main(int ac, const char **av)
{
  char line[MAXLINE + 1];
  int ilineno = 0;
  enum state state = state_normal;

  size_t nonws_chars_out = 0;

  while (fgets(line, sizeof line, stdin)) {
    ++ilineno;
    size_t n = strlen(line);
    if (n == 0) {
      // final line in file lacks newline
      continue;
    }
    if (n > sizeof line - 2) {
      fprintf(stderr, "warning: line %d too long (max %zu bytes)\n", ilineno,
              sizeof line - 2);
      return 1;
    }
    if (line[n - 1] != '\n') {
      fprintf(stderr, "warning: line %d lacks a newline\n", ilineno);
      line[n] = '\n';
      line[n + 1] = '\0';
    }

    // reset the nonwhitespace counter for each line, unless we are in a
    // multiline comment or string
    //
    switch (state) {
    default:
      nonws_chars_out = 0;
      break;
    case state_comment:
    case state_squote:
    case state_dquote:
      break;
    }

    int last_char_output = 0;
#   define OUTC_COUNT(C)    do { fputc(last_char_output = (C), stdout); ++nonws_chars_out; } while (0)
#   define OUTC_NOCOUNT(C)  do { fputc(last_char_output = (C), stdout); } while (0)

    for (const char *cp = line; *cp; ++cp) {
      int ch = cp[0];
      int next_ch = cp[1]; // note: string will always end in \0

      // CSS level 3 section 3.3: Preprocessing the input stream:
      // (note that NULL CHARACTER replacements are impossible since we use
      // null-terminated C strings)
      //
      switch (ch) {
      case '\r':
        if (next_ch == '\n') {
          continue; // CRLF -> LF
        } else {
          ch = '\n'; // standalone CR -> LF
        }
        break;
      case '\f':
        ch = '\n'; // FF -> LF (where on earth did that come from)
        break;
      }

      switch (state) {
      // note: this is a goto label, not a case; it allows any other state in
      // the machine to easily switch back to normal state and reprocess the
      // same character
      //
      reprocess_as_normal:
        state = state_normal;
        // fallthrough

      case state_normal:
        if (ch == '\n') {
          if (nonws_chars_out) {
            OUTC_NOCOUNT(ch);
          }
        } else if (is_hws(ch)) {
          // don't print anything yet; wait till the end of the state (this
          // allows for end-of-line trimming)
          state = state_hws;
        } else if (ch == '/' && next_ch == '*') {
          state = state_startcomment;
        } else if (ch == '\'') {
          OUTC_COUNT(ch);
          state = state_squote;
        } else if (ch == '"') {
          OUTC_COUNT(ch);
          state = state_dquote;
        } else {
          OUTC_COUNT(ch);
        }
        break;

      case state_hws:
        if (is_hws(ch)) {
          // consume all hws characters until the end, where we produce one
          // only if needed

        } else if (ch == '\n') {
          // reprocess eol as normal, but don't output anything
          goto reprocess_as_normal;

        } else if (ch == '/' && next_ch == '*') {
          // reproces start comment as normal, but don't output anything
          goto reprocess_as_normal;

        } else {
          // non-hws, non-eol character: reprocess as normal, but first, if we
          // there are any non-ws characters already out, and a space was not
          // previously written (due to e.g. " /**/ " construct), insert a
          // single space to delimit tokens
          //
          if (nonws_chars_out && !is_hws(last_char_output)) {
            OUTC_NOCOUNT(' ');
          }
          goto reprocess_as_normal;
        }
        break;

      case state_dquote:
        OUTC_COUNT(ch);
        if (ch == '\\') {
          state = state_bquote_in_dquote;
        } else if (ch == '\n') {
          // note: the 'bquote' state takes care of escaped newlines
          fprintf(stderr, "warning: line %d contains an unterminated string\n",
                  ilineno);
          state = state_normal;
        } else if (ch == '"') {
          state = state_normal;
        }
        break;

      case state_squote:
        OUTC_COUNT(ch);
        if (ch == '\\') {
          state = state_bquote_in_squote;
        } else if (ch == '\n') {
          // note: the 'bquote' state takes care of escaped newlines
          fprintf(stderr, "warning: line %d contains an unterminated string\n",
                  ilineno);
          state = state_normal;
        } else if (ch == '\'') {
          state = state_normal;
        }
        break;

      case state_bquote_in_dquote:
        // note: we *only* care here about escaped quotes and escaped
        // newlines, so we only need to inspect the first character of the
        // escape, even if it's a hex escape \1234 which technically encodes
        // just one character
        //
        OUTC_COUNT(ch);
        state = state_dquote;
        break;

      case state_bquote_in_squote:
        // please see comment under state_bquote_in_dquote
        //
        OUTC_COUNT(ch);
        state = state_squote;
        break;

      case state_startcomment:
        // determined by lookahead in state_normal
        //
        state = state_comment;
        break;

      case state_comment:
        if (ch == '*' && next_ch == '/') {
          state = state_endcomment;
        }
        break;

      case state_endcomment:
        // determined by lookahead in state_startcomment
        //
        // comment is concluded; treat it as hws and let the normal hws
        // rules take effect as to whether anything is actually printed
        //
        state = state_hws;
        break;
      }
    }
  }

  switch (state) {
  default:
    // valid state for eof
    break;

  case state_dquote:
  case state_squote:
  case state_bquote_in_dquote:
  case state_bquote_in_squote:
    fprintf(stderr, "warning: unterminated string at eof\n");
    break;

  case state_comment:
    fprintf(stderr, "warning: unterminated comment at eof\n");
    break;
  }
}

