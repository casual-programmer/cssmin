
# cssmin: simple CSS minifier

`cssmin` is a plain lexical CSS compressor. It performs purely lexical
compression, removing comments, whitespace, etc., and does not parse or
rewrite CSS files or attempt to remove redundancy. I wrote it to shrink and
strip comments from my small, hand-authored CSS files, with a minimum of
dependencies and things to configure. If you are a more sophisticated Web
developer, you probably want a more sophisiticated CSS preprocessor/minifier
which is able to do more than pure textual transforms.

Copyright (c) 2017 Aaron Trickey <aaron@casualprogrammer.com>

## Usage

    cssmin < input.css > output.css

There are no arguments and no options. Input is read only from stdin, and
output is written only to stdout.

As a side effect, this program will produce warnings for a couple of CSS
lexical errors; however, most lexical errors will be ignored. It is not
suitable for CSS validation.

This program should work for any UTF-8-encoded input. It does not support any
other encoding. If fed data embedding a U+0000 NULL CHARACTER, i.e. a binary
zero byte, some characters will be lost, even though technically the CSS spec
allows this and defines a replacement.

This program has a maximum input line length of 1KB, including newline.  Lines
longer than this will be split at 1KB and will result in a warning being
printed.

## Textual removals/transformations

- Each comment is treated as a single horizontal whitespace (which then may be
  minified per the space rules below)
- Any CRLF, standalone CR, and FF characters are converted to plain LFs, per
  the CSS preprocessing rules.
- All empty lines or whitespace-only lines are removed.
- All leading horizontal whitespace (indentation) is removed.
- All trailing horizontal whitespace is removed.
- All spans of horizontal whitespace within any line are replaced by single
  space characters.

Note: spaces, newlines, and comment syntax are of course left unmolested
within quotes (double or single). CR and FF preprocessing still occurs.

## Building

`cssmin` is written in a single C file. It has no dependencies other than the
standard C library, and should be extremely portable, although I've only
tested it on macOS with Clang.

A Makefile is provided. Type `make` to compile `cssmin`.

The script `test.sh` is a simple Bash script containing a variety of tests
used during the development of `cssmin`. Run it directly, or type `make test`,
to verify any changes you make.



