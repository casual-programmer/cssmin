
CFLAGS = -Wall -Wextra -Werror -Wno-unused-parameter

cssmin:		cssmin.c
		$(CC) $(CFLAGS) -o $@ $<

test:		cssmin test.sh
		bash test.sh

