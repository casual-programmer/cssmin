#!/bin/bash

bold=$(tput bold)
sgr0=$(tput sgr0)

expect_warning() {
  echo "$1" | ./cssmin >_test.out 2>_test.err
  if grep -q warning _test.err ; then
    echo "ok: got warning: $1"
  else
    echo "${bold}fail${sgr0}: no warning: $1"
  fi
}

expect() {
  echo "$1" | ./cssmin >_test.out 2>_test.err
  if grep -q warning _test.err ; then
    echo "${bold}fail${sgr0}: $1 ($(cat _test.err))"
  elif [ "$(cat _test.out)" = "$2" ] ; then
    echo "ok: $1"
  else
    echo "${bold}fail${sgr0}: $1 ('$(cat _test.out)' ≠ '$2')"
  fi
}

# unterminated strings
expect_warning "abc ' def"
expect_warning 'abc " def'

# strings with escapes
expect "' \\' '"            "' \\' '"
expect '" \" "'             '" \" "'
expect '" \
 "'                         '" \
 "'

# leading & trailing whitespace
expect "   abc"             "abc"
expect "abc   "             "abc"
expect "  abc  "            "abc"

# embedded whitespace
expect "abc def"            "abc def"
expect "abc   def"          "abc def"
expect "abc 	 def"         "abc def" # there's a tab in there
expect "a	b	c"              "a b c"   # tabs in there too
expect " a b "              "a b"     # trailing whitespace trimmed as well

# comments
expect "/* */"              ""
expect "/**/"               ""
expect "/* */abc"           "abc"
expect " /* */ abc"         "abc"
expect "abc/* */"           "abc"
expect "abc /* */ "         "abc"
expect "a/**/b"             "a b"     # require a space here to delimit tokens
expect "a /**/ b"           "a b"     # collapse spaces around tokens
expect "/*abc*/def"         "def"
expect "abc /*
*/ def"                     "abc def"
expect "//**//"             "/ /"
expect "abc/* *///* */def"  "abc / def"
expect_warning "/*"                   # unterminated comment
expect_warning "/*/"                  # unterminated comment
expect "a/*/*/b"            "a b"
expect "a/***/b"            "a b"

# newline preprocessing
expect "a
b"                          "a
b"                                    # CRLF
expect "ab"               "a
b"                                    # standalone CR

# characters outside the ASCII range
# (this file is utf8)
expect " 😀 Δ € "            "😀 Δ €"


